import java.util.Arrays;
import java.util.Random;
//binary search
class BinarySearchComparison{

    public static void main(String[] args) {
        int repetition = 100; //change repetition here
        int [] data ;
        Random random  = new Random();
        int input [] = generateRandomArray(100000000);
        data =  Sort(input);
        int find = data[random.nextInt(data.length)];
        int searchKeyArray[] = shuffle(data);
        long start = System.currentTimeMillis();
        long end = start + 60*1000; // one minute 60*1000

        int count = 0;
        for (int  i=0;i<searchKeyArray.length;i++){
            //for recursive binary seach uncomment below commented lines and comment iterative one.
            //for iteravive comment other as is
            //this is done so that we could measure individual speed
            if ((System.currentTimeMillis() ) < end){ //100000000
                // int found =BinarySearch(data,searchKeyArray[i],0,input.length-1);
                int found =iterativeBinarySearch(data,searchKeyArray[i],0,input.length-1);
                //do not comment code below it is for counting both
                if(found == -1 || found > 0 ){
                    count = count+1;
                }
            }

        }
        System.out.println("Total Search in 1 minute  "+count);


    }


    // shuffle array
    public static int[] shuffle(int[] ary){
        //System.out.println("Shuffled");
        Random random = new Random();
        int size = ary.length;
        int temp;
        for (int i = 0; i < size; i++)
        {
            int j = random.nextInt(size);

            temp = ary[i];
            ary[i] = ary[j];
            ary[j] = temp;
        }
        return ary;
    }

    /**
     * Generate Random Array of size n.
     *
     * param int
     * return int[]
     */
    public static int[] generateRandomArray(int n){
        //System.out.println("Random Array");
        int random_array[] = new int[n];
        Random random = new Random();
        for (int i = 0; i < n; i++)
        {
            random_array[i] = random.nextInt(n);
        }
        return random_array;
    }

    /**
     * Binary Search.
     *
     * param int
     * return int[]
     */
    public  static int BinarySearch(int arary[], int searchKey, int low, int high){

        if(high < low ){
            return -1;
        }
        int mid = low + ((high - low) / 2);
        if (arary[mid] > searchKey ){

            return BinarySearch(arary, searchKey, low, mid-1);
        }else if (arary[mid] < searchKey ){

            return BinarySearch(arary,searchKey,mid+1, high);
        }else {
            return mid;
        }

    }

    public static int iterativeBinarySearch(int array[], int searchKey,int low, int high){
        int mid = 0;

        while (low < high ){
            mid = low+((high-low)/2);
            if (array[mid] == searchKey){
                return mid;
            }
            if (array[mid] < searchKey){
                low = mid+1;
            }else  {
                high = mid-1;
            }
        }
        return -1;
    }
    /**
     * Sort the unsorted array.
     *
     * param int[]
     * return int[]
     */

    public static int[] Sort(int [] unsortedData){
        //System.out.println("Sorted");
        int [] sortedData = unsortedData;
        Arrays.sort(sortedData);
        return sortedData;
    }

}