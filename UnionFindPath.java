// Ref: https://www.coursera.org/learn/algorithms-part1/
// https://algs4.cs.princeton.edu/15uf/
// Dependencies: StdIn.java StdOut.java from princeton 
public class UnionFindPath {
    private int[] id;    // id[i] = parent of i
    public  int count;   // number of components
    public  int total = 0;

    public UnionFindPath(int n) {
        count = n;
        id = new int[n];
        for (int i = 0; i < n; i++) {
            id[i] = i;
        }
    }


    public int find(int p) {
        int root = p;
        while (root != id[root])
            root = id[root];
        while (p != root) {
            int newp = id[p];
            id[p] = root;
            p = newp;
        }
        return root;
    }


    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }


    public void union(int p, int q) {
        int left = find(p);
        int right = find(q);
        if (left == right) return;
        id[left] = right;
        total ++;
        count--;
    }


    public static void main(String[] args) {
        //external dependencies used from alg4s
        int totalInput = StdIn.readInt(); //read from file
        UnionFindPath uf = new UnionFindPath(totalInput);
        long start = System.currentTimeMillis();
        while (!StdIn.isEmpty()) {
            int p = StdIn.readInt();
            int q = StdIn.readInt();
            if (uf.connected(p, q)) continue;
            uf.union(p, q);
            StdOut.println(p + " " + q);
        }
        long end = System.currentTimeMillis() - start;

        StdOut.println(uf.total + "total Successful count");
        StdOut.println(uf.count + " unsuccessful count");
    }

}