/*

    Comparision of Sorting performance of Inplace merge sort Vs Normal merge sort

 */ 

import java.util.Random;

class InPlaceMergeSort {
    public static void main(String[] args) {


        int repetition = 100;

        int size = 1;
        long start = System.nanoTime();
        for (int i = 1; i <= repetition; i++){
            size = i * 1000;
            //inplace merge sort uncomment it and comment merge sort
//            inplaceMergeSort(generateRandomArray(size),0, size-1);
            //normal merge sort
            mergeSort(generateRandomArray(size),0,size-1);
            long end = System.nanoTime();
            double nanoseconds = ((end - start) );
            System.out.println(size+"  "+nanoseconds);

        }

    }
    /**
     * Generate Random Array of size n.
     *
     * param int
     * @return int[]
     */
    public static int[] generateRandomArray(int n){
        int random_array[] = new int[n];
        Random random = new Random();
        for (int i = 0; i < n; i++)
        {
            random_array[i] = random.nextInt(n);
        }
        return random_array;
    }
    //code for inplace merge sort
    public static void inplaceMergeSort(int []InputArray, int start, int end){
        int mid, left, right, temp;
        if (start >= end){ //nothing to sort
            return;
        }
        mid = (start+end)/2;

        inplaceMergeSort(InputArray,start, mid);
        inplaceMergeSort(InputArray,mid+1,end);
        left = start; right = mid+1;

        if (InputArray[mid] <= InputArray[right]  ){
            return;
        }
        while (left <= mid && right <= end){
            if (InputArray[left] <= InputArray[right]){
                left++;
            }else {
                temp = InputArray[right];
                System.arraycopy(InputArray, left,InputArray,left+1,right-left);
                InputArray[left] = temp;
                left++;mid++;right++;
            }
        }
    }

    // normal merge sort
    public static void mergeSort(int [] array,int lowerIndex, int higherIndex) {

        if (lowerIndex < higherIndex) {
            int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
            mergeSort(array,lowerIndex, middle);
            mergeSort(array,middle + 1, higherIndex);
            merge(array, lowerIndex, middle, higherIndex);
        }
    }

    public static void merge(int [] array, int lowerIndex, int middle, int higherIndex) {
        int temp[] = new int[array.length];
        for (int i = 0; i <= higherIndex; i++) {
            temp[i] = array[i];
        }
        int i = lowerIndex;
        int j = middle + 1;
        int k = lowerIndex;
        while (i <= middle && j <= higherIndex) {
            if (temp[i] <= temp[j]) {
                array[k] = temp[i];
                i++;
            } else {
                array[k] = temp[j];
                j++;
            }
            k++;
        }
        while (i <= middle) {
            array[k] = temp[i];
            k++;
            i++;
        }

    }

}
