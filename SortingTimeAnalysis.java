
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/*
 * Sorting Time Analysis 
 * Bubble sort is taken for this
 */
public class SortingTimeAnalysis {
    public static void main(String[] args) {
        //Number of repetition: 100, 1000, 3000, (Integer.MAX_VALUE)
        int repetition = 100;
        int randomArray[] = {1, 9, 6, 4, 5 };
        long start = currentTime();
        getInversionCount(randomArray);
        // bubbleSort(randomArray);
        long end = currentTime();
        double nanoseconds = ((end - start) );
        // printResult(getInversionCount(randomArray), nanoseconds);

        // for (int i = 1; i <= repetition; i++){
        //     randomArray = new int[i*10];
        //     randomArray = generateRandomArray(i * 10);
        //     bubbleSort(randomArray);
        //     long end = currentTime();
        //     double nanoseconds = ((end - start) );
        //     printResult(getInversionCount(randomArray), nanoseconds);
        // }
    }

    /**
     * Display a result.
     *
     * param int, double
     * return void
     */
    public static void printResult( int  repetition, double time){

        System.out.println((repetition) +" "+ time );

    }

    /**
     * Sort the unsorted data i.e. Bubble Sort.
     *
     * param int[]
     * return int[]
     */
    public static int[] bubbleSort(int[] unSortedData){
        int arraySize = unSortedData.length;
        int sortedData[] = unSortedData;
        boolean swapped;
        int temp = 0;
        for (int i = 0; i < arraySize; i++ ){
            swapped = false;
            for (int j = 0; j < arraySize - i - 1; j++){
                if(sortedData[j] > sortedData[j+1]){
                    temp = sortedData[j];
                    sortedData[j] = sortedData[j+1];
                    sortedData[j+1] = temp;
                    swapped = true;
                }
            }
            if(swapped == false){
                break;
            }
        }

        return sortedData;
    }

    /**
     * Generate Random Array of size n.
     *
     * param int
     * return int[]
     */
    public static void getInversionCount(int [] array_unsorted){
        int inversionCount = 0;

        for (int i = 0; i < array_unsorted.length - 1; i++)
        {
            for (int j = i + 1; j < array_unsorted.length; j++) {
                if (array_unsorted[i] > array_unsorted[j]) {
                    inversionCount++;
                }
            }
        }
        System.out.println(inversionCount);
        //return inversionCount;
    }

    /**
     * Generate Random Array of size n.
     *
     * param int
     * return int[]
     */
    public static int[] generateRandomArray(int n){
        int random_array[] = new int[n];
        Random random = new Random();
        for (int i = 0; i < n; i++)
        {
            random_array[i] = random.nextInt(n);
        }
        return random_array;
    }

    /**
     * Calculate current time in nano seconds.
     *
     * return long
     */
    public static long currentTime(){
        long  time;
        time = System.nanoTime();
        return time;
    }
}
