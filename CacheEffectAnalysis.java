
public class CacheEffectAnalysis {

    public static void main(String[] args) {

        final int ROW_1 = 3, ROW_2 = 3;
        final int COLUMN_1 = 3, COLUMN_2 = 3;
        int[][] MatrixA = { {3, 6, 5}, {3, 3, 4},{2, 3, 4} };
        int[][] MatrixB = { {2, 3,4}, {9, 7,10}, {2, 4,5} };

        multiplyMatrices(MatrixA,MatrixB,ROW_1,COLUMN_1,COLUMN_2);

    }

    public static void multiplyMatrices(int[][] matrixA, int[][] matrixB, int row1, int column1, int column2) {
        int[][] product = new int[row1][column2];
        long start = System.nanoTime();
        //Access order ijk change the access order to jik to see change
        for(int i = 0; i < row1; i++) {
            for (int j = 0; j < column2; j++) {
                for (int k = 0; k < column1; k++) {
                    product[i][j] += matrixA[i][k] * matrixB[k][j];
                }
            }
        }

        System.out.println("Time:"+(System.nanoTime() - start));
    }


}
