
/*
 * This Dual Pivot Quick Sort is implemented based on the algorithm by Vladimir Yaroslavskiy
 * Author: Tek Raj Chhetri
 * Link to Article: https://arxiv.org/pdf/1306.3819.pdf
 * */

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

class DualPivotQuickSort{

    public static void main(String[] args) {
        int size ;
        int [] inputArray ;
        long start = System.nanoTime();
        for (int i = 1; i <= 100; i++){
            size = i * 1000;
            inputArray = new int[size];
            inputArray = generateRandomArray(size);

            DualPivotSort(inputArray, 0 , inputArray.length- 1);
            // DefaultSort(inputArray);

            long end = System.nanoTime();
            double nanoseconds = ((end - start) );
            System.out.println(size+"  "+nanoseconds);

//            for (int ji = 0; ji < inputArray.length; ji++){
//                System.out.println(inputArray[ji]);
//            }

        }



    }
    public static void swap(int [] array, int i, int j){
        int tmp = array[i]; array[i] = array[j]; array[j] = tmp;
    }


    //implementation for dual pivot quick sort based on algorithm in https://arxiv.org/pdf/1306.3819.pdf article
    public static void DualPivotSort(int A[], int left, int right){
        if (left <= right){
            //p and q represent left pivot
            int p, q;

            if(A[left] > A[right]){
                if (A[left] > A[right]) swap(A, left, right);
            }
            //pivot
            p = A[left]; q = A[right];
            // for bad pivot uncomment the below commented
//            int midForPivot = (left+right)/2; // for bad pivot
//            p = A[midForPivot]; q = A[midForPivot-left];

            int l = left +1, g = right - 1, k = l;
            while (k <= g){
                if (A[k] < p){
                    swap(A, k ,l);
                    l = l + 1;
                }else{
                    if (A[k] >= q){
                        while (A[g] > q && k < g) g = g - 1; //end while
                        if (A[g] >= p){
                            swap(A,k ,g );
                        }else{
                            swap(A,k,g);
                            swap(A,k,l);
                            l = l + 1;
                        }
                        g = g - 1;
                    }
                }
                k = k +1;
            }
            l = l-1; g = g +1;
            swap(A,left,l);
            swap(A,right,g);



            // Recursively sort partitions
            DualPivotSort(A, left, l - 1);
            DualPivotSort(A, l + 1, g - 1);
            DualPivotSort(A, g + 1, right);

        }

    }

    /**
     * Generate Random Array of size n.
     *
     * param int
     * @return int[]
     */
    public static int[] generateRandomArray(int n){
        int random_array[] = new int[n];
        Random random = new Random();
        for (int i = 0; i < n; i++)
        {
            random_array[i] = random.nextInt(n);
        }
        return random_array;
    }



    public static void DefaultSort(int []array){
        Arrays.sort(array);

    }
}